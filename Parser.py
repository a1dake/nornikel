import sys
from datetime import datetime
import requests
import cfscrape
import urllib3
from urllib3 import exceptions
import sqlite3
import re
import os
sys.path.append("/home/manage_report")
currentdir = os.path.dirname(os.path.realpath(__file__))
db_path = os.path.join(currentdir, "nornikel.db")
sys.path.append(db_path)
from Send_report.mywrapper import magicDB

class Parser:
    def __init__(self, parser_name: str):
        self.session = cfscrape.create_scraper(sess=requests.Session())
        self.result_data: dict = {'name': parser_name,
                                  'data': []}

    @magicDB
    def run(self):
        content: list = self.get_data()
        self.result_data['data'] = content
        print(content)
        return self.result_data

    def create_database(self):
        try:
            conn = sqlite3.connect(db_path)
            sql = conn.cursor()
            sql.execute("""CREATE TABLE orders (url text);""")
            conn.close()
        except:
            pass

    def insert_data(self, value):
        conn = sqlite3.connect(db_path)
        c = conn.cursor()
        c.execute("INSERT INTO orders(url) VALUES (?)", [value])
        conn.commit()
        conn.close()

    def search_data(self, search_value):
        conn = sqlite3.connect(db_path)
        c = conn.cursor()
        c.execute("SELECT * FROM orders WHERE url = ?", (search_value,))
        rows = c.fetchall()
        if rows:
            conn.close()
            return True
        else:
            conn.close()
            return False

    def get_data(self):
        urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
        session = requests.Session()

        links = ['https://www.nornickel.ru/ajax/tenders-centralized.php',
                 'https://www.nornickel.ru/ajax/tenders-local.php']
        session.headers.update({
            'Accept': 'application/json, text/javascript, */*; q=0.01',
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64)'
                          ' AppleWebKit/537.36 (KHTML, like Gecko)'
                          ' Chrome/109.0.0.0 Safari/537.36 Edg/109.0.1518.55'
                    })
        session.get('https://www.nornickel.ru/', verify=False)

        json_data = 'draw=1&columns%5B0%5D%5Bdata%5D=subject_procurement&columns%5B0%5D%5Bname%5D=&columns%5B0%5D%5Bsearchable%5D=true' \
                    '&columns%5B0%5D%5Borderable%5D=false&columns%5B0%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B0%5D%5Bsearch%5D%5Bregex%5D=false' \
                    '&columns%5B1%5D%5Bdata%5D=method_carrying&columns%5B1%5D%5Bname%5D=&columns%5B1%5D%5Bsearchable%5D=true' \
                    '&columns%5B1%5D%5Borderable%5D=false&columns%5B1%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B1%5D%5Bsearch%5D%5Bregex%5D=false' \
                    '&columns%5B2%5D%5Bdata%5D=deadline&columns%5B2%5D%5Bname%5D=&columns%5B2%5D%5Bsearchable%5D=true' \
                    '&columns%5B2%5D%5Borderable%5D=false&columns%5B2%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B2%5D%5Bsearch%5D%5Bregex%5D=false' \
                    '&columns%5B3%5D%5Bdata%5D=how_apply&columns%5B3%5D%5Bname%5D=&columns%5B3%5D%5Bsearchable%5D=true' \
                    '&columns%5B3%5D%5Borderable%5D=false&columns%5B3%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B3%5D%5Bsearch%5D%5Bregex%5D=false' \
                    '&columns%5B4%5D%5Bdata%5D=customer&columns%5B4%5D%5Bname%5D=&columns%5B4%5D%5Bsearchable%5D=true&columns%5B4%5D%5Borderable%5D=false' \
                    '&columns%5B4%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B4%5D%5Bsearch%5D%5Bregex%5D=false' \
                    '&columns%5B5%5D%5Bdata%5D=address&columns%5B5%5D%5Bname%5D=&columns%5B5%5D%5Bsearchable%5D=true&columns%5B5%5D%5Borderable%5D=false' \
                    '&columns%5B5%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B5%5D%5Bsearch%5D%5Bregex%5D=false' \
                    '&columns%5B6%5D%5Bdata%5D=organizer&columns%5B6%5D%5Bname%5D=&columns%5B6%5D%5Bsearchable%5D=true&columns%5B6%5D%5Borderable%5D=false' \
                    '&columns%5B6%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B6%5D%5Bsearch%5D%5Bregex%5D=false' \
                    '&columns%5B7%5D%5Bdata%5D=contacts&columns%5B7%5D%5Bname%5D=&columns%5B7%5D%5Bsearchable%5D=true&columns%5B7%5D%5Borderable%5D=false' \
                    '&columns%5B7%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B7%5D%5Bsearch%5D%5Bregex%5D=false' \
                    '&start=0&length=100&search%5Bvalue%5D=&search%5Bregex%5D=false&sort=active_from'
        lists = []
        for link in links:
            response = session.post(link, json=json_data, verify=False)
            data = response.json()
            list = data.get('data')
            lists += list
        contents = self.get_content(lists)
        return contents

    def get_content(self, data):
        self.create_database()
        orders = []
        for item in data:
            item_data = {
                'type': 2,
                'title': '',
                'purchaseNumber': '',
                'fz': 'Коммерческие',
                'purchaseType': '',
                'url': '',
                'attachments': [],
                'procedureInfo': {
                    'endDate': ''
                },
                'customer': {
                    'fullName': 'ПАО \"ГМК \"Норильский никель\"',
                    'factAddress': 'Красноярский край, Таймырский Долгано-Ненецкий район, город Дудинка, ул. Морозова, д. 1',
                    'inn': 8401005730,
                    'kpp': 840101001,
                }
            }

            title, purchase_number = self.get_title(item)
            item_data['title'] = str(title)
            item_data['purchaseNumber'] = str(purchase_number)
            item_data['purchaseType'] = str(self.get_purchase_type(item))
            item_data['procedureInfo']['endDate'] = date = self.get_end_date(item)

            item_data['attachments'] = self.get_files(item)

            url = ('https://www.nornickel.ru/' + re.sub(r'[^\w\s]', '', ''.join(title.split()))[0:30] + re.sub(r'[^\w\s]', '', date)).lower()

            item_data['url'] = str(url)

            db_search = self.search_data(url)
            if db_search:
                continue
            else:
                orders.append(item_data)
                self.insert_data(url)

        return orders

    def get_title(self, data):
        try:
            title = ''.join(data.get("subject_procurement").split('target="_blank">')[1]).split('</a><span class')[0]
            number = title.split(':')[0]
            pattern = re.compile(r"^\d+/\d", re.IGNORECASE)
            if pattern.match(number):
                purchase_number = number
                title = title.split(purchase_number)[1][2:]
            else:
                purchase_number = ''
        except Exception as e:
            print(e)
            title = ''
            purchase_number = ''
        return str(title), str(purchase_number)

    def get_purchase_type(self, data):
        try:
            purchase_type = data.get("method_carrying")
        except:
            purchase_type = ''
        return str(purchase_type)

    def get_end_date(self, data):
        try:
            date = data.get("deadline").strip('<nobr>').strip('</nobr>')
            formatted_date = self.formate_date(date)
        except:
            formatted_date = None
        return formatted_date

    def get_files(self, data):
        try:
            docs_data = []
            desc = ''.join(data.get("subject_procurement").split('target="_blank">')[1]).split('</a><span class')[0]
            link = 'https://www.nornickel.ru/' + ''.join(data.get("subject_procurement").split('<a href="')[1]).split('"')[0]
            doc = {'docDescription': desc,
                   'url': link}
            docs_data.append(doc)
        except:
            return []
        return docs_data

    def formate_date(self, old_date):
        date_object = datetime.strptime(old_date, '%d.%m.%Y')
        formatted_date = date_object.strftime('%H.%M.%S %d.%m.%Y')
        return formatted_date
