# -*- coding: utf-8 -*-
import settings
import logging
from datetime import datetime
from Parser import Parser


if __name__ == "__main__":
    start_time = datetime.now()
    parser = Parser(
        parser_name="nornikel"
    )

    try:
        parser.run()
        finish_time = datetime.now()
        logging.info(f"Парсер отработал за {finish_time - start_time}")
        exit(0)
    except Exception as err:
        finish_time = datetime.now()
        logging.error(f"Парсер упал с ошибкой: {err}")
        logging.info(f"Парсер отработал до ошибки за время {finish_time - start_time}")
        exit(1)
